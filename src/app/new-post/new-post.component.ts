import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ManagePostsService } from '../services/manage-posts/manage-posts.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {

  constructor(private managePost: ManagePostsService, private router: Router) {}

  onSubmit(form: NgForm) {
    const newPost = {
      id: this.managePost.getLastId(),
      title: form.value['title'],
      contain: form.value['contain'],
      like: 0,
      dislike: 0
    };
    this.managePost.addPost(newPost);
    this.router.navigate(['/posts']);
  }

  ngOnInit() {
  }

}

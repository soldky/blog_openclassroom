import { Component, Input, OnInit } from '@angular/core';
import { ManagePostsService } from '../services/manage-posts/manage-posts.service';

import { AppComponent } from '../app.component';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  @Input() public postId: number;
  @Input() public postTitle: string;
  @Input() public postContain: string;
  @Input() public lastUpdate: Date;
  @Input() public like: number;
  @Input() public dislike: number;

  constructor(protected managePost: ManagePostsService) { }

  ngOnInit() {
  }

}

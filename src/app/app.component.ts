import { Component, OnDestroy, OnInit } from '@angular/core';
import { ManagePostsService } from './services/manage-posts/manage-posts.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  protected title = 'blog';
  protected lastUpdate = new Date();
  protected posts = [];
  protected postsSubscription: Subscription;

  constructor(protected managePosts: ManagePostsService, protected router: Router) {}

  ngOnInit() {
    this.postsSubscription = this.managePosts.postsSubject.subscribe(
      (posts: any[]) => {
        this.posts = posts;
      }
    );
    this.managePosts.emitPostSubject();
  }

  ngOnDestroy() {
    this.postsSubscription.unsubscribe();
  }

}

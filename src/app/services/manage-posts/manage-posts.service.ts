import { Subject } from 'rxjs';

export class ManagePostsService {

  public postsSubject = new Subject<any[]>();
  private posts = [
    {
      title: "Post 1",
      contain: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      like: 0,
      dislike: 0
    },
    {
      id: 2,
      title: "Post 2",
      contain: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      like: 0,
      dislike: 0
      },
    {
      id: 3,
      title: "Post 3",
      contain: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      like: 0,
      dislike: 0
    }
	];

  public emitPostSubject() {
    this.postsSubject.next(this.posts.slice());
  }

  public addPost(post: any) {
    this.posts.push(post);
    this.emitPostSubject();
  }

  public removePost(id: number) {
    for (let postIndice = 0; postIndice < this.posts.length; postIndice++) {
      if (this.posts[postIndice].id === id) {
        this.posts.splice(postIndice, 1);
      }
    }
    this.emitPostSubject();
  }

  public addLike(id: number) {
    for (let postIndice = 0; postIndice < this.posts.length; postIndice++) {
      if (this.posts[postIndice].id === id) {
        this.posts[postIndice].like += 1;
      }
    }
  	this.emitPostSubject();
  }

  public addDislike(id: number) {
  	for (let postIndice = 0; postIndice < this.posts.length; postIndice++) {
      if (this.posts[postIndice].id === id) {
        this.posts[postIndice].dislike += 1;
      }
    }
  	this.emitPostSubject();
  }

  public getLastId() {
    let lastId = 0;
    this.posts.forEach(function(post) {
      if (post.id > lastId) {
        lastId = post.id;
      }
    });
    return lastId + 1;
  }

}

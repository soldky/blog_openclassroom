import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import { PostComponent } from './post/post.component';

import { ManagePostsService } from './services/manage-posts/manage-posts.service';
import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from './menu/menu.component';
import { NewPostComponent } from './new-post/new-post.component';

const appRoutes: Routes = [
  { path: 'posts', component: AppComponent },
  { path: 'new', component: NewPostComponent },
  { path: '',   redirectTo: '/posts', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    PostComponent,
    MenuComponent,
    NewPostComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule
  ],
  providers: [
    ManagePostsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
